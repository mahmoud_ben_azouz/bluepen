<?php

namespace App\Modules\Employees\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Employees\Models\Employee;
use Illuminate\Http\Request;
use PDF;

class EmployeesController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        return view("Employees::employees", compact('employees'));
    }

    /**
     * export list employees PDF
     *
     * @return \Illuminate\Http\Response
     */
    public function export(Employee $employee)
    {

        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif', 'isPhpEnabled' => true]);
        $pdf = PDF::loadView('pdf.pdf_employees', compact('employee'));
        return $pdf->download('employees.pdf');

    }
}

@extends('index')
@section('content')
<div class="demo-html" style="margin: 25px;">
    <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5">
        <div class="row">
            <div class="col-sm-12">
                <table id="example" class="table table-striped dataTable" style="width: 100%;" aria-describedby="example_info">
                    <thead>
                        <tr>
                            <th class="sorting sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="" >Fistname</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" >Lastname</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" >Email</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" >Site</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" >Job Position</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" >Phone</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" >Contrat</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" >Salary</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" >Fils</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" >Dat of Birth</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" >Start date</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" >End date</th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" >Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($employees as $employee)
                        <tr class="odd">
                            <td class="sorting_1">{{$employee->first_name}}</td>
                            <td>{{$employee->last_name}}</td>
                            <td>{{$employee->email}}</td>
                            <td>{{$employee->site}}</td>
                            <td>{{$employee->job_position}}</td>
                            <td>{{$employee->phone}}</td>
                            <td>{{$employee->type_contrat}}</td>
                            <td>{{$employee->salary}}</td>
                            <td>{{$employee->number_fils}}</td>
                            <td>$162,700</td>
                            <td>$162,700</td>
                            <td>$162,700</td>
                            <td>
                                <button class="btn btn-warning" title="Exporter les réponses" href=""><i class="fa fa-edit"></i></button>
                                <button class="btn btn-danger"  title="Exporter les réponses" href=""><i class="fa fa-trash"></i></button>
                                <a href="{{url('export/{$employee->id}')}}" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i></a>
                            
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
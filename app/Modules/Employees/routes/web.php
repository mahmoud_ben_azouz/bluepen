<?php

use Illuminate\Support\Facades\Route;

Route::get('employees', 'EmployeesController@index');
Route::get('/export', 'EmployeesController@export');


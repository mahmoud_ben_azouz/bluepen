<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BluePen Labs</title>
    <link rel="icon" type="image/x-icon" href="{{asset('images/favicon.png')}}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
</head>

<header class="antialiased">
    <nav class="navbar navbar-expand navbar-dark bg-dark">
        <div class="collapse navbar-collapse justify-content-md-center" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="/employees">Employee management</a>
                <a class="nav-item nav-link active" href="#">Contract management</a>
                <a class="nav-item nav-link active" href="#">Payment</a>
            </div>
        </div>
    </nav>

</header>

<body>
    @yield('content')
</body>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    })
</script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script> 
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script> 
<script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>

<!-- <script>
    $('.contact-form').each(function() {
        $(this).validate({
            submitHandler: function(form) {

                var $form = $(form),
                    $brut = $form.find('#brut'),
                    $submitButton = $(this.submitButton),
                    submitButtonText = $submitButton.val(),
                    $abattementEnfant = 0,
                    $abattementCf = 0,
                    $leSalaire = 0,
                    $impot = 0;



                $submitButton.val($submitButton.data('loading-text') ? $submitButton.data('loading-text') : 'En cours...').attr('disabled', true);

                // Fields Data
                var formData = $form.serializeArray(),
                    data = {};

                $(formData).each(function(index, obj) {
                    data[obj.name] = obj.value;
                });

                var $salaireBrut = parseInt(data.salaire * 1000) / 1000,
                    $chef = data.chef,
                    $enfant = parseInt(data.enfant);

                //abattement CF
                if ($chef === 'on') {
                    $abattementCf = 300;
                } else {
                    $abattementCf = 0;
                }

                //abattement enfant à charge
                if ($enfant > 0) {
                    $abattementEnfant = ($enfant * 100);
                } else {
                    $abattementEnfant = 0;
                }

                $leSalaire = ($salaireBrut * 0.9082 * 12);

                if (($leSalaire * 0.1) > 2000) {
                    $leSalaire = ($leSalaire - 2000);
                } else {
                    $leSalaire = ($leSalaire * 0.9);
                }

                $leSalaire = $leSalaire - $abattementEnfant - $abattementCf;


                if ($leSalaire < 5000) {
                    $impot = ($leSalaire * 0.01);
                } else {
                    if ($leSalaire < 20000) {
                        $impot = (($leSalaire - 5000) * 0.27) + 50;
                    } else {
                        if ($leSalaire < 30000) {
                            $impot = (($leSalaire - 20000) * 0.29) + 4100;
                        } else {
                            if ($leSalaire < 50000) {
                                $impot = (($leSalaire - 30000) * 0.33) + 7000;
                            } else {
                                if ($leSalaire >= 50000) {
                                    $impot = (($leSalaire - 50000) * 0.36) + 13600;
                                }
                            }
                        }
                    }
                }


                $("#brut").val(parseFloat($salaireBrut).toFixed(3));


                $("#cnss").val(parseFloat(Math.round(($salaireBrut * 0.0918) * 1000) / 1000).toFixed(3));
                $("#imposable").val(parseFloat(Math.round(($salaireBrut * 0.9082) * 1000) / 1000).toFixed(3));
                $("#rs").val(parseFloat(Math.round((($impot / 12) - ($leSalaire * 0.01 / 12)) * 1000) / 1000).toFixed(3));
                $("#css").val(parseFloat(Math.round(($leSalaire * 0.01 / 12) * 1000) / 1000).toFixed(3));
                $("#net").val(parseFloat($("#imposable").val() - $("#rs").val() - $("#css").val()).toFixed(3));
            }
        })
    })
</script> -->